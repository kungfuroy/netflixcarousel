//
//  PlaylistViewController.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import UIKit

class PlaylistViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var isBusy: Bool = false {
        didSet {
            if oldValue {
                activityIndicator.stopAnimating()
            } else {
                activityIndicator.startAnimating()
            }
        }
    }
    
    fileprivate lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .gray)
        indicator.hidesWhenStopped = true
        return indicator
    }()

    private var playlists: [Playlist]? {
        get {
            return FeedManager.sharedInstance.playlists
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Theme Sample"
        
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
        
        collectionView.register(PlaylistCell.self)
        collectionView.register(UINib(nibName: "HeaderReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderReusableView.identifier)
        
        isBusy = true
        FeedManager.getPlaylist {[weak self] (success) in
            self?.isBusy = false
            if success {
                self?.collectionView.reloadData()
            }
        }
    }
}

extension PlaylistViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return playlists?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PlaylistCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.update(playlist: playlists?[indexPath.section], for: indexPath)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView: HeaderReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderReusableView.identifier, for: indexPath) as! HeaderReusableView
        headerView.playlist = playlists?[indexPath.section]
        return headerView
    }

}

extension PlaylistViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let playlist = playlists?[indexPath.section] else {
            return .zero
        }
        return (playlist.currentLayout() == .landscape ? CGSize(width: collectionView.bounds.width, height: 160) : CGSize(width: collectionView.bounds.width, height: 250.0) )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 50.0)
    }
}

extension PlaylistViewController: PlaylistCellDelegate {
    func didTapOnAlbum(_ album: Album?, with indexPath: IndexPath) {
        let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlbumDetailsViewController") as! AlbumDetailsViewController
        detailVC.album = album
        navigationController?.pushViewController(detailVC, animated: true)
    }
}
