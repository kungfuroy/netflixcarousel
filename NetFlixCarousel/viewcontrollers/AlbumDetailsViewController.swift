//
//  AlbumDetailsViewController.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 14/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import UIKit
import Foundation

class AlbumDetailsViewController: UIViewController {

    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var playerHolder: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var album: Album? {
        didSet {
            update()
        }
    }
    
    var isBusy: Bool = false {
        didSet {
            if oldValue {
                activityIndicator.stopAnimating()
            } else {
                activityIndicator.startAnimating()
            }
        }
    }
        
    fileprivate var player: PlayerView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        player = PlayerView(frame: playerHolder.bounds)
        playerHolder.addSubview(player!)
    }
    
    fileprivate func update() {
        guard let album = album, let identifier = album._id else {
            return
        }
        
        loadViewIfNeeded()
        
        let titleAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 18.0),
            .foregroundColor: UIColor.black
        ]

        let descriptionAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14.0, weight: .medium),
            .foregroundColor: UIColor.black
        ]
        
        let attributedTitle = NSAttributedString(string: album.displayTitle(), attributes: titleAttributes)
        let attributedDescription = NSAttributedString(string: album.ottDescription ?? "", attributes: descriptionAttributes)
        
        let description = NSMutableAttributedString(attributedString: attributedTitle)        
        description.append(NSAttributedString(string: "\n\n"))
        description.append(attributedDescription)
        
        descriptionTextView.attributedText = description
        
        if album.locked() {
            purchaseButton.isHidden = false
            let title = purchaseButton.titleLabel?.text ?? ""
            if let price = album.purchasePrice {
                purchaseButton.setTitle(title + "$ " + price, for: .normal)
            }
        } else {
            purchaseButton.isHidden = true
            isBusy = true
            FeedManager.getVideoURL(for: Configuration.videoURL(for: identifier)!) {[weak self] (details) in
                self?.isBusy = false
                guard let details = details, let output = details.body?.outputs, output.count > 0 else {
                    return
                }
                guard let url = output[0].url, let mediaURL = URL(string: url) else {return}
                self?.player?.play(mediaURL: mediaURL)
            }
        }
        
    }
}
