//
//  FeedManager.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import Foundation

class FeedManager: NSObject {
    
    static let sharedInstance = FeedManager()
    
    var playlists: [Playlist]?
    
    class func getPlaylist(completion: @escaping((_ success: Bool) -> ())) {
        Service.getData(with: URL(string: Configuration.Url.kPlaylistUrl)!) { (data) in
            guard let responseData = data , let root = try? JDecoder.jsonDecoder().decode(PlaylistRoot.self, from: responseData), let playlist = root.response else {
                completion(false)
                return
            }
            sharedInstance.playlists = playlist
            completion(true)
        }
    }
    
    class func getAlbum(for playlistId:String, completion:@escaping((_ success: Bool, _ albums: [Album]?) -> ())) {
        Service.getData(with: Configuration.playlistItemURL(for: playlistId)!) { (data) in
            guard let responseData = data , let root = try? JDecoder.jsonDecoder().decode(AlbumRoot.self, from: responseData), let albums = root.response else {
                completion(false, nil)
                return
            }
            completion(true, albums)
        }
    }
    
    class func getVideoURL(for URL: URL, completion: @escaping((_ videoDetails: VideoDetails?)->())) {
        Service.getData(with: URL, userAgent: true) { (data) in
            guard let responseData = data , let root = try? JDecoder.jsonDecoder().decode(VideoDetailsRoot.self, from: responseData), let videoDetails = root.response else {                
                completion(nil)
                return
            }
            completion(videoDetails)
        }
    }
    
}


