//
//  Feed.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import Foundation
import UIKit

class JDecoder {
    class func jsonDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }
}

protocol JCodable: Codable {
    func toJSONString() -> String
}

extension JCodable {
    func toJSONString() -> String {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        guard let data = try? encoder.encode(self) else {return "Could not convert."}
        return String(bytes: data, encoding: .utf8)!
    }
}

struct PlaylistRoot : JCodable {
    let pagination : Pagination?
    let response : [Playlist]?
}

struct Pagination: JCodable {
    let current : Int = 0
    let pages : Int = 0
    let perPage : Int = 0
}

struct Playlist: JCodable {
    let _id : String?
    let _keywords : [String]?
    let active = false
    let autoRemoveVideoEntitlements = false
    let autoUpdateVideoEntitlements = false
    let createdAt : String?
    let deletedAt : String?
    let descriptionField : String?
    let discoveryUrl : String?
    let friendlyTitle : String?
    let matchType : String?
    let parentId : String?
    let playlistItemCount : Int = 0
    let playlistType : String?
    let priority : Int = 0
    let purchaseRequired = false
    let rentalRequired = false
    let siteId : String?
    let thumbnailLayout : String?
    let thumbnails : [String]?
    let title : String?
    let updatedAt : String?
    
//    enum CodingKeys: String, CodingKey {
//        case id = "_id"
//        case keywords = "_keywords"
//    }
    
    
    
    //MARK: TODO --- Unknown Datatypes
    
    //let images : [AnyObject]?
    //let purchasePrice : AnyObject?
    //let contentRules : [AnyObject]?
    //let relatedVideoIds : [String]?
    //let rentalDuration : AnyObject?
    //let rentalPrice : AnyObject?
}

enum ThumbLayout {
    case portrait, landscape
}

extension Playlist {
    func currentLayout() -> ThumbLayout{
        guard let layout = thumbnailLayout else {
            return .landscape
        }
        
        switch layout {
        case "portrait":
            return .portrait
        case "landscape":
            return .landscape
        default:
            return .landscape
        }
    }
    
    func currentLayoutSize() -> CGSize {
        return (currentLayout() == .landscape ? ThemeConfiguration.Netflix.landscapeSize : ThemeConfiguration.Netflix.portraitSize)
    }
}


struct AlbumRoot: JCodable {
    let pagination : Pagination?
    let response : [Album]?
}


struct Album: JCodable {
    let _id : String?
    let active = false
    let country : String?
    let createdAt : String?
    let customAttributes : CustomAttribute?
    let descriptionField : String?
    let discoveryUrl : String?
    let duration : Int = 0
    let featured = false
    let freeAudioStream = false
    let friendlyTitle : String?
    let images : [Cover]?
    let isZypeLive = false
    let marketplaceIds : MarketplaceId?
    let matureContent = false
    let onAir = false
    let ottDescription : String?
    let passRequired = false
    let purchasePrice : String?
    let purchaseRequired = false
    let rating : Int = 0
    let registrationRequired = false
    let rentalDuration : Int = 0
    let rentalPrice : String?
    let rentalRequired = false
    let requestCount : Int = 0
    let shortDescription : String?
    let siteId : String?
    let sourceId : String?
    let status : String?
    let subscriptionAdsEnabled = false
    let subscriptionRequired = false
    let thumbnails : [Thumbnail]?
    let title : String?
    let transcoded = false
    let updatedAt : String?

//MARK: TODO --- Unknown Datatypes
    
//    let mrssId : AnyObject?
//    let enableAt : AnyObject?
//    let episode : AnyObject?
//    let huluId : AnyObject?
//    let kalturaId : AnyObject?
//    let keywords : [AnyObject]?
//    let crunchyrollId : AnyObject?
//    let disableAt : AnyObject?
//    let foreignId : AnyObject?
//    let planIds : [AnyObject]?
//    let previewIds : [AnyObject]?
//    let programGuideId : AnyObject?
//    let publishedAt : AnyObject?
//    let relatedPlaylistIds : [AnyObject]?
//    let season : AnyObject?
//    let vimeoId : AnyObject?
//    let youtubeId : AnyObject?
//    let zobjectIds : [AnyObject]?
//    let contentRules : [AnyObject]?
    
}

extension Album {
    func displayTitle() -> String {
        guard let title = title else {return ""}
        return title
    }
    
    func locked() -> Bool {
        return (registrationRequired || purchaseRequired || rentalRequired || purchasePrice != nil )
    }
}

struct Thumbnail : JCodable {
    let aspectRatio : Float = 0.0
    let height : Int = 0
    let name : String?
    let url : String?
    let width : Int = 0
}


struct Cover : JCodable {
    let id : String?
    let aspectRatio : Float = 0.0
    let caption : String?
    let height : Int  = 0
    let layout : String?
    let title : String?
    let updatedAt : String?
    let url : String?
    let width : Int = 0
}

struct CustomAttribute : Codable {
}

struct MarketplaceId : Codable {
}

struct AlbumDetailsRoot {
    let response : AlbumDetails?
}

struct AlbumDetails: JCodable {
    let id : String?
    let active : Bool = false

    let country : String?
    let createdAt : String?

    let customAttributes : CustomAttribute?
    let descriptionField : String?

    let discoveryUrl : String?
    let duration : Int?
    let featured : Bool = false

    let freeAudioStream : Bool = false
    let friendlyTitle : String?

    let isZypeLive : Bool = false
    let marketplaceIds : MarketplaceId?
    let matureContent : Bool = false

    let onAir : Bool = false
    let ottDescription : String?
    let passRequired : Bool = false
    let purchasePrice : String?
    let purchaseRequired : Bool = false
    let rating : Int?
    let registrationRequired : Bool = false
    
    let rentalDuration : Int?
    let rentalPrice : String?
    let rentalRequired : Bool = false
    let requestCount : Int?
    
    let shortDescription : String?
    let siteId : String?
    let sourceId : String?
    let status : String?
    let subscriptionAdsEnabled : Bool = false
    let subscriptionRequired : Bool = false
    let thumbnails : [Thumbnail]?
    let title : String?
    let transcoded : Bool = false
    let updatedAt : String?
    
    //MARK: TODO --- Unknown Datatypes
    
//    let mrssId : AnyObject?
//    let disableAt : AnyObject?
//    let crunchyrollId : AnyObject?
//    let foreignId : AnyObject?
//    let huluId : AnyObject?
//    let enableAt : AnyObject?
//    let episode : AnyObject?

//    let contentRules : [AnyObject]?
//    let kalturaId : AnyObject?
//    let keywords : [AnyObject]?

//    let planIds : [AnyObject]?
//    let previewIds : [AnyObject]?
//    let programGuideId : AnyObject?
//    let publishedAt : AnyObject?

//    let relatedPlaylistIds : [AnyObject]?
//    let season : AnyObject?
//    let vimeoId : AnyObject?
//    let youtubeId : AnyObject?
//    let zobjectIds : [AnyObject]?
}

extension AlbumDetails {
    func locked() -> Bool {
        return (registrationRequired || purchaseRequired || rentalRequired)
    }
}


struct VideoDetailsRoot: JCodable {
    let response: VideoDetails?
}

struct VideoDetails: JCodable {
    let body : Body?
    let device : Device?
    let player : Player?
    let provider : Provider?
    let revenueModel : RevenueModel?
    let video : Video?
}

struct Video : JCodable {
    
    let id : String?
    let active : Bool?
//    let contentRules : [AnyObject]?
    let country : String?
    let createdAt : String?
//    let crunchyrollId : AnyObject?
    let customAttributes : CustomAttribute?
    let descriptionField : String?
//    let disableAt : AnyObject?
    let discoveryUrl : String?
    let duration : Int?
//    let enableAt : AnyObject?
//    let episode : AnyObject?
    let featured : Bool?
//    let foreignId : AnyObject?
    let freeAudioStream : Bool?
    let friendlyTitle : String?
//    let huluId : AnyObject?
    let isZypeLive : Bool?
//    let kalturaId : AnyObject?
//    let keywords : [AnyObject]?
    let marketplaceIds : MarketplaceId?
    let matureContent : Bool?
//    let mrssId : AnyObject?
    let onAir : Bool?
    let ottDescription : String?
    let passRequired : Bool?
    let purchaseRequired : Bool?
    let rating : Int?
    let registrationRequired : Bool?
    let rentalRequired : Bool?
    let requestCount : Int?
//    let season : AnyObject?
    let shortDescription : String?
    let siteId : String?
    let sourceId : String?
    let status : String?
    let subscriptionAdsEnabled : Bool?
    let subscriptionRequired : Bool?
    let thumbnails : [Thumbnail]?
    let title : String?
    let transcoded : Bool?
    let updatedAt : String?
    
//    let relatedPlaylistIds : [AnyObject]?
//    let rentalDuration : AnyObject?
//    let rentalPrice : AnyObject?
//    let planIds : [AnyObject]?
//    let previewIds : [AnyObject]?
//    let programGuideId : AnyObject?
//    let publishedAt : AnyObject?
//    let purchasePrice : AnyObject?

//    let vimeoId : AnyObject?
//    let youtubeId : AnyObject?
//    let zobjectIds : [AnyObject]?
    
}

struct RevenueModel : Codable {
    
    let id : String?
    let descriptionField : String?
    let name : String?
}

struct Provider : Codable {
    
    let id : String?
    let name : String?
}

struct Player : Codable {
    let id : String?
    let name : String?
}

struct Device : Codable {
    
    let id : String?
    let descriptionField : String?
    let name : String?
}


struct Body : Codable {
    
    let analytics : Analytic?
    let files : [File]?
    let onAir : Bool?
    let outputs : [Output]?
//    let subtitles : [AnyObject]?

}


struct Output : Codable {
    let name : String?
    let url : String?
}

struct File : Codable {
    
    let name : String?
    let url : String?
}

struct Analytic : Codable {
    
    let beacon : String?
    let dimensions : Dimension?

}

struct Dimension : Codable {
    
//    let device : AnyObject?
//    let playerId : AnyObject?
    let siteId : String?
    let videoId : String?

}
