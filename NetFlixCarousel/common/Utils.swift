//
//  Utils.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import UIKit

typealias JSONObject = [String:AnyObject]

protocol ReusableView {
    static var defaultIdentifier: String {get}
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

protocol NibReusableView: class {
    static var nibName: String { get }
}

extension NibReusableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}


extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView {
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView, T: NibReusableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        
        register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        return cell
    }
}


func onMainThread(block: @escaping ()->()) {
    DispatchQueue.main.async(execute: block)
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}


extension UIView{
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    
    @IBInspectable var borderWidth:CGFloat {
        set {
            
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    func debugBounds() {
        self.borderColor = UIColor.randomColor()
        self.borderWidth = 1.0
    }
}


extension Data {
    func dataToPrettyJSON() -> String? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: .mutableContainers) as? JSONObject else { return nil }
        return object.json()
    }
    
    func dataToJson() -> JSONObject? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: .mutableContainers) as? JSONObject else { return nil }
        return object
    }
}

extension Collection {
    func json() -> String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: [.prettyPrinted])
            guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
                return "{}"
            }
            return jsonString
        } catch let parseError {
            return "{}"
        }
    }
}
