//
//  Service.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import Foundation


class Service {    
    class func getData(with URL: URL, userAgent: Bool = false, completion:@escaping((_ data: Data?) -> ())) {
        var request = URLRequest(url: URL)
        if userAgent {
            request.setValue("User-Agent", forHTTPHeaderField: Configuration.agent)
        }
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            onMainThread {
                guard error == nil, let responseData = data else {
                    completion(nil)
                    return
                }
                completion(responseData)
            }
        }
        task.resume()
    }
}



