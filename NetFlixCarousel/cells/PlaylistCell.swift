//
//  PlaylistCell.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import UIKit

protocol PlaylistCellDelegate: NSObjectProtocol {
    func didTapOnAlbum(_ album: Album?, with indexPath:IndexPath)
}

class PlaylistCell: UICollectionViewCell, NibReusableView, ReusableView {

    static let defaultIdentifier = PlaylistCell.defaultReuseIdentifier
    static let nibName = PlaylistCell.nibName
    
    fileprivate (set) var albums: [Album]?
    fileprivate (set) var currentPlaylist: Playlist?
    
    weak var delegate: PlaylistCellDelegate?

    @IBOutlet weak var noContentLabel: UILabel!
    @IBOutlet weak var albumsCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var isBusy: Bool = false {
        didSet {
            if oldValue {
                activityIndicator.stopAnimating()
            } else {
                activityIndicator.startAnimating()
            }
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
        albumsCollectionView.dataSource = self
        albumsCollectionView.delegate =  self
        albumsCollectionView.register(AlbumCell.self)
        albumsCollectionView.contentInset  = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func update(playlist: Playlist?,for indexPath: IndexPath) {
        currentPlaylist = playlist
        albumsCollectionView.isHidden = false
        noContentLabel.isHidden = true
        isBusy = true
        FeedManager.getAlbum(for: playlist?._id ?? "") {[weak self] (success, albums) in
            self?.isBusy = false
            guard success, let albums = albums, albums.count > 0 else {
                self?.albumsCollectionView.isHidden = true
                self?.noContentLabel.isHidden = false
                return
            }
            self?.albums = albums
            self?.albumsCollectionView.reloadData()
        }
    }

}


extension PlaylistCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: AlbumCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.album = albums?[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTapOnAlbum(albums?[indexPath.row], with: indexPath)
    }
}

extension PlaylistCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let playlist = currentPlaylist else {
            return .zero
        }
        return playlist.currentLayoutSize()
    }
}
