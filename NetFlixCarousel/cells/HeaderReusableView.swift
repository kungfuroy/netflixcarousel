//
//  HeaderReusableView.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 13/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import UIKit

class HeaderReusableView: UICollectionReusableView {

    static let identifier = "HeaderReusableView"   
    @IBOutlet weak var headerLabel: UILabel!
    
    var playlist: Playlist? {
        didSet {
            update()
        }
    }
    
    fileprivate func update() {
        guard let playlist = playlist else {return}
        headerLabel.text = playlist.title ?? ""
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
