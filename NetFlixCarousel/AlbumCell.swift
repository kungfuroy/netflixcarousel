//
//  AlbumCell.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import UIKit
import SDWebImage

class AlbumCell: UICollectionViewCell, NibReusableView, ReusableView {
    
    static let defaultIdentifier = AlbumCell.defaultReuseIdentifier
    static let nibName = AlbumCell.nibName
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewHtContraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
        
    var album: Album? {
        didSet {
            update()
        }
    }
    
    fileprivate func update() {
        guard let album = album  else { return }
        print("Album Id: \(album._id)")
        if let thumbs = album.thumbnails, thumbs.count > 0 {
            let thumb = thumbs[0]
            if let url = thumb.url, let URL = URL(string: url) {
                imageView.sd_setImage(with: URL, completed: nil)
            }
        }
        
        if let images = album.images, images.count > 0 {
            let image = images[0]
            if let url = image.url, let URL = URL(string: url) {
                imageView.sd_setImage(with: URL, completed: nil)
            }
        }
        
        titleLabel.text = album.displayTitle()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
