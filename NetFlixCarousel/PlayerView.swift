//
//  PlayerView.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 15/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import Foundation
import AVFoundation
import AVKit


class PlayerView: UIView {
    
    private (set) lazy var playerLayer: AVPlayerLayer = {
        let layer = AVPlayerLayer()
        return layer
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    fileprivate var mediaURL:URL!
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        playerLayer.frame = bounds
        layer.addSublayer(playerLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func play(mediaURL: URL) {
        self.mediaURL = mediaURL
        playerLayer.player = AVPlayer(url: mediaURL)
        playerLayer.videoGravity = .resize
        playerLayer.player?.play()
    }
    
    func pause() {
        playerLayer.player?.pause()
    }
    
}


