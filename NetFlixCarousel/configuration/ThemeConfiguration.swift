//
//  ThemeConfiguration.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 13/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import Foundation
import UIKit

struct ThemeConfiguration {
    struct Netflix {
        static let landscapeSize = CGSize(width: 178.0, height: 150.0)
        static let portraitSize = CGSize(width: 100.0, height: 178.0)
    }
}
