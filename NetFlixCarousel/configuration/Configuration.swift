//
//  Configuration.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import Foundation

class Configuration {
    
    static let productionAppKey = "eIqvZ7K4fJLZgQvWGH8oolqKq5G-J1oUPtY07nTunWlZnuOeahcPhqATaXr3zmi_"
    static let kStaging = false
    static let agent = "Dalvik/2.1.0 (Zype Android; Linux; U; Android 5.0.2; One X Build/LRX22G)"
    
    struct Url {
        static let kStagingServerUrl = ""
        static let kProductionServerUrl = "https://api.zype.com/"
        static let kPlaylistUrl = Url.serverUrl() + "playlists?app_key=" + Configuration.appKey()
        static let kPlaylistItemUrl = Url.serverUrl()  + "playlists/<id>/" + "videos?app_key=" + Configuration.appKey()
        static let kVideoUrl = Url.serverUrl() + "embed/<id>?app_key=" + Configuration.appKey()
        
        static func serverUrl() -> String {
            if Environment.isTest() {
                return kStagingServerUrl
            }
            return kProductionServerUrl
        }
    }
}


extension Configuration {
    class func appKey() -> String {
        if Environment.isTest() {
            return ""
        }
        return  Configuration.productionAppKey
    }
    
    class func playlistItemURL(for id:String) -> URL? {
        let urlString = Configuration.Url.kPlaylistItemUrl.replacingOccurrences(of: "<id>", with: id)
        return URL(string: urlString)
    }
    
    class func videoURL(for id: String) -> URL? {
        let urlString = Configuration.Url.kVideoUrl.replacingOccurrences(of: "<id>", with: id)
        return URL(string: urlString)
    }
}

