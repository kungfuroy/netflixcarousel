//
//  Environment.swift
//  NetFlixCarousel
//
//  Created by Sumanta Roy on 12/05/19.
//  Copyright © 2019 Sumanta Roy. All rights reserved.
//

import Foundation

struct Environment {
    
    static func isTest() -> Bool {
        return Configuration.kStaging
    }
    
}
